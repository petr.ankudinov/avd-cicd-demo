#!/bin/bash
#
# Purpose: CICD Installation script
# Author: @titom73, Petr Ankudinov
#   based on https://get.avd.sh/atd/install.sh
# Date: 2021-06-06
# Update: 2021-06-06
# Version: 0.1
# License: Apache 2.0
# --------------------------------------

# Update PATH to match ATD Jumphost setting
export PATH=$PATH:$HOME/.local/bin/
export PYTHONPATH=$PYTHONPATH:$HOME/.local/lib/python3.8/site-packages/

# Bin path
_CURL=$(which curl)
_GIT=$(which git)
_GITLAB_RUNNER=$(which gitlab-runner)

# Local Installation Path
_INSTALLATION_PATH="labfiles/arista-ansible"
_ROOT_INSTALLATION_DIR="${PWD}/${_INSTALLATION_PATH}"

# List of Arista Repositories
_REPO_AVD="https://github.com/aristanetworks/ansible-avd.git"
_REPO_CVP="https://github.com/aristanetworks/ansible-cvp.git"
_REPO_EXAMPLES="https://gitlab.com/petr.ankudinov/avd-cicd-demo.git"
_REPO_EXAMPLES_BRANCH="atd-cicd-0.1"

# Path for local repositories
_LOCAL_AVD="${_ROOT_INSTALLATION_DIR}/ansible-avd"
_LOCAL_CVP="${_ROOT_INSTALLATION_DIR}/ansible-cvp"
_LOCAL_EXAMPLES="${_ROOT_INSTALLATION_DIR}/atd-avd"

# Get latest stable version from github
_AVD_VERSION=$(curl -L -I -s -o /dev/null -w %{url_effective} https://github.com/aristanetworks/ansible-avd/releases/latest | cut -d "/" -f 8)
_CVP_VERSION=$(curl -L -I -s -o /dev/null -w %{url_effective} https://github.com/aristanetworks/ansible-cvp/releases/latest | cut -d "/" -f 8)

# Print post-installation instructions
info_installation_done() {
    echo ""
    echo "Installation done."
    echo ""
    echo "You can access setup at ${_ROOT_INSTALLATION_DIR} to review all the files: collections and playbooks"
    echo "You can login to AVD environment with:"
    echo "--------------------------------------"
    echo "   cd ${_LOCAL_EXAMPLES}"
    echo "   < start playing with AVD content >"
    echo "--------------------------------------"
    echo ""
    echo "For complete documentaiton, visit: https://github.com/arista-netdevops-community/atd-avd"
    echo ""
}


##############################################
# Main content for script
##############################################

echo "Arista Ansible AVD installation is starting"

# Test if git is installed
if hash git 2>/dev/null; then
    echo "  * git has been found here: " $(which git)
else
    echo "  ! git is not installed, installation aborted"
    exit 1
fi

echo "  * Deployed AVD environment"
if [ ! -d "${_ROOT_INSTALLATION_DIR}" ]; then
    echo "  * creating local installation folder: ${_ROOT_INSTALLATION_DIR}"
    mkdir -p ${_ROOT_INSTALLATION_DIR}
    echo "  * cloning ansible-avd collections (${_AVD_VERSION}) to ${_LOCAL_AVD}"
    ${_GIT} clone --depth 1 --branch ${_AVD_VERSION} ${_REPO_AVD} ${_LOCAL_AVD} > /dev/null 2>&1
    echo "  * cloning ansible-cvp collections (${_CVP_VERSION}) to ${_LOCAL_CVP}"
    ${_GIT} clone --depth 1 --branch ${_CVP_VERSION} ${_REPO_CVP} ${_LOCAL_CVP} > /dev/null 2>&1
    echo "  * cloning netdevops-examples collections to ${_LOCAL_EXAMPLES}"
    ${_GIT} clone ${_REPO_EXAMPLES} ${_LOCAL_EXAMPLES} > /dev/null 2>&1
    # switch to v3 branch
    cd ${_LOCAL_EXAMPLES}
    ${_GIT} checkout ${_REPO_EXAMPLES_BRANCH}

    # Create inventory file
    cp ${_LOCAL_EXAMPLES}/atd-inventory/inventory-without-credentials.yml ${_LOCAL_EXAMPLES}/atd-inventory/inventory.yml 2>&1
    # Replace passwords with the provided ATD token password
    echo "  * replacing inventory passwords"
    sed -i "s/# update password with \"Lab Credentials\"/${atd_password}/g" ${_LOCAL_EXAMPLES}/atd-inventory/inventory.yml

    # Placeholder for specific ATD repos where we can position some topologies

else
    echo "  ! local installation folder already exists - ${_ROOT_INSTALLATION_DIR}"
    # switch to v3 branch
    cd ${_LOCAL_EXAMPLES}
    ${_GIT} checkout ${_REPO_EXAMPLES_BRANCH}
    echo "  * executing git pull"
    ${_GIT} pull
fi

# Install and register CICD runner
echo "Installing gitlab-runner"
curl -sL "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash > /dev/null 2>&1
export GITLAB_RUNNER_DISABLE_SKEL=true; sudo -E apt-get install gitlab-runner > /dev/null 2>&1
echo "Registering and starting gitlab-runner"
sudo gitlab-runner register \
--non-interactive \
--url "https://gitlab.com/" \
--registration-token "${cicd_runner_token}" \
--executor "shell" \
--description "shell-runner-on-atd" \
--tag-list "atd,cicd_demo" \
--run-untagged="true" > /dev/null 2>&1
sudo gitlab-runner start > /dev/null 2>&1
sudo gitlab-runner verify > /dev/null 2>&1

# install other packages
sudo apt-get install yamllint
sudo apt-get install ansible-lint

info_installation_done
