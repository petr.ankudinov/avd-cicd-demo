# AVD CICD Demo

A simple CICD demo for AVD

## Overview

This repository can be used to demonstrate simple CICD workflow using [`arista.avd`](https://github.com/aristanetworks/ansible-avd), [`arista.cvp`](https://github.com/aristanetworks/ansible-cvp) and GitLab CICD.
To simplify deployment this repository is relying on [`ATD.AVD`](https://github.com/arista-netdevops-community/atd-avd) and only adds GitLab CICD pipeline. Please read [`ATD.AVD` documentation](https://github.com/arista-netdevops-community/atd-avd/blob/master/DEMO.md) first.

## How to Start

1. Read [`ATD.AVD` documentation](https://github.com/arista-netdevops-community/atd-avd/blob/master/DEMO.md) documentation. This README file is created under assumption that everything explained in ATD.AVD documentation is clear to CICD demo user.
2. Request ATD token, start the topology and access `Programmability IDE`.
3. Change directory to `labfiles`.
4. Run `export atd_password="atd_passwd"; export cicd_runner_token="cicd_token"; curl -fsSL https://gitlab.com/petr.ankudinov/avd-cicd-demo/-/raw/atd-cicd-0.1/cicd_demo_setup.sh?inline=false | sh`
5. Change to atd-avd directory: `cd labfiles/arista-ansible/atd-avd`
6. Prepare CloudVision for AVD: `ansible-playbook playbooks/atd-prepare-lab.yml` This step is required to create all containers. If that was done earlier, it can be skipped.
7. Run `ansible-playbook playbooks/atd-fabric-deploy.yml` if the fabric was not yet deployed.

## License

The project is published under [Apache License](https://choosealicense.com/licenses/apache-2.0/).
